# parse nasm listing and generate definitions for kernel code

function hex(s) {
    x = 0;
    for(i = 1; i <= length(s); i++) {
        c = substr(s, i, 1);
        x = (x*16) + hext[c];
    }
    return x
}

BEGIN {
    for(i = 0; i < 16; i++) hext[sprintf("%X",i)] = i;
    base = hex(mbase)
    word = ""
    const = ""
    offset = 0
    system("rm -f addrs.f")
}

$2 ~ /^[[:xdigit:]]+$/ {
    off = base + hex($2)
    if(word != "") print off, "kcode", word
    else if(const != "") print off, ("constant '" const) >> "addrs.f"
    word = ""
    const = ""
}

$2 == ";=" {const = $3; word = ""}
$2 == ";" {word = $3; const = ""}
