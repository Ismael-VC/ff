\ glob-pattern matching

defer rmatch   ( pa pu -- f )                                   
2variable mstate                                                
: mlen  ( -- u ) mstate @ ;                                     
: fetch  ( a1 u1 -- a2 u2 c ) over c@ >r  1 /string  r> ;       
: next  ( -- c ) mstate 2@ fetch -rot  mstate 2! ;              
: empty  ( -- f )  mlen 0= ;                                    
: mcont  ( pa pu -- f ) 2r> 2drop  rmatch ;                     
: match1  ( pa pu c -- f ) empty  if  r> 2drop 2drop  false  |  
  next =  if  mcont  |  2drop  false ;                          
: match?  ( pa pu -- f ) empty  if  r> drop  2drop  false  |    
  next drop  mcont ;                                            
: match*  ( pa pu -- f ) ?dup 0=  if  drop  true  |             
  begin  mstate 2@  2over rmatch  if  2drop  2drop  true  |     
    mstate 2!  next drop  again ;                               
: setfetch  ( pa pu c1 -- pa' pu' c1 c2 )                       
  >r  over c@ >r  1 /string  r> r> swap ;                       
: setskip  ( pa pu -- pa' pu' )                                 
  [char] ] scan  ?dup 0= abort" missing ]"  1 /string ;         
: (match[-])  ( pa pu c1 c2 -- pa' pu' f )                      
  2>r  dup 2 < abort" invalid pattern"                          
  fetch  2r> rot 1+ within ;                                    
: (match[])  ( pa pu c1 c2 -- pa' pu' f )                       
  2>r  over c@  [char] - =  if  1 /string  2r>  (match[-])      
  else  2r> =   then  if  setskip  true  |  false ;             
: match[]  ( pa pu -- f ) next                                  
  begin  dup >r  over 0=  abort" bad pattern"                   
    setfetch  [char] ]  ->  r> 2drop 2drop  false  |            
    (match[])  if  2r> 2drop  rmatch  |  r>  again ;            
: domatch  ( pa pu -- f ) r> drop  fetch                        
  [char] *  ->  match*   |                                      
  [char] [  ->  match[]  |                                      
  [char] ?  ->  match?  |  match1 ;                             
: (rmatch)   ( pa pu -- f ) ?dup 0=  if  drop  empty  |  domatch ; 
' (rmatch) is rmatch                                            
: match  ( a u pa pu -- f ) 2swap mstate 2! rmatch ;            
