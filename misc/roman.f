\ print roman numbers

create vals  1000 ,  900 ,  500 ,  400 ,  100 ,  90 ,  50 ,  40 ,  10 ,
  9 ,  5 ,  4 ,  1 ,
: .rsym  ( u -- ) 
  0  ->  ." M"  |  1  ->  ." CM"  |  2  ->  ." D"  |  3  ->  ." CD"  |
  4  ->  ." C"  |  5  ->  ."  XC"  |  6  ->  ." L"  |  7  ->  ." XL"  |
  8  ->  ." X"  |  9  ->  ." IX"  |  10  ->  ." V"  |  11  ->  ." IV"  |
  drop  ." I" ;
variable num
: .roman  ( u -- ) num !
  0  begin  num @ while  
    vals over th @ num @ swap /  0  ?do
      dup .rsym  vals over th @ negate num +!  loop
    1+  repeat
  drop ;
