\ DOER/MAKE (from "Thinking Forth")

: doer  ( | <word> -- ) <builds  ['] noop >body ,  does>  @ >r ;
: does-pfa  ( xt -- pfa ) 2 cells + ;

variable mmarker

: (make)  ( [skip word ...] -- )
    r> dup cell+ dup cell+ swap @ does-pfa ! 
    @ ?dup  if >r then ;

: make  ( | <word> ... -- )
    state @ if
        postpone (make)  here mmarker !  0 ,
    else
        here ' does-pfa !  ]
    then ; immediate

: ;and  ( | ... -- ) postpone exit  here mmarker @ ! ; immediate
: undo  ( | <word> -- ) ['] noop >body ' does-pfa ! ;
