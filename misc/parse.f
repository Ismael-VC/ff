\ simple parsing


.( parse )

variable input      variable output     variable ap     
variable limit

256 constant #maxstates
#maxstates cells buffer: states
variable stateptr   

1024 constant #maxargs
#maxargs cells buffer: pstack
states stateptr !   pstack ap !

: fragment  ( a -- a n )
    dup 70 0 do
        dup limit @ = over c@ 10 = or if  drop i unloop exit  then  
    1+  loop  drop 70 ; 

: savestate  stateptr @ input @ !+  output @ !+  ap @ !+  stateptr ! ;
: dropstate  -3 cells stateptr +! ;
: restorestate  stateptr @ 3 cells - @+ input !  @+ output !  @ ap ! ;
: >a  ( x -- ) ap @ !  1 cells ap +! ;
: a@  ( -- x ) ap @ -1 cells + @ ;
: a>  ( -- x ) -1 cells ap +!  ap @ @ ;
: {  ( f -- f ) dup if  savestate  else  r> drop  then ;
: }  ( f -- f ) dup 0= if  restorestate  then  dropstate ;
: |  ( f -- f ) if  dropstate  r> drop  else  restorestate  then  true ;

: failed  ( a n -- )
    type ." : " type cr
    input @ fragment type cr  abort ;

: atend?  ( -- f ) input @ limit @ < 0= ;
: next  ( -- c ) atend? if  0  else  input @ utfdecode nip  then ;
: skip  input @ utfdecode drop input ! ;
: ws?  ( c -- f ) dup bl =  swap 9 14 within or ;
: alpha?  ( c -- f ) dup 256 < and 32 invert and  [char] A [char] [ within ;
: numeric?  ( c -- f ) [char] 0 [char] : within ;

: special?  ( c -- f ) 
    dup 0= if  exit  then
    dup alpha? swap numeric? or 0= ;

defer <ws>

: (<ws>)  begin  next ws? while  skip  repeat ;

: (match)  ( a n -- f )
    <ws>  atend? if  2drop  false  exit  then
    dup >r  input @ over compare 0= if
        r> input +!  true
    else  r> drop false  then ;

: match  ( f1 a n -- f2 ) rot if  (match)  else  2drop false  then ;

: «  ( f | ...» -- f' )
    [char] » utfparse count sliteral  postpone match ; immediate

: initparse  ( a n -- ) swap input !  input @ + limit ! ;
: parsestate@  ( -- a1 a2 ) input @ limit @ ;
: parsestate!  ( a1 a2 -- ) limit ! input ! ;

' (<ws>) is <ws>
