\ simple Cheney-style semispace GC

.( gc )

256 constant maxroots

maxroots cells buffer: roots  variable #roots

variable fspace-start   variable fspace-limit   variable fspace-top
variable tspace-start   variable tspace-limit   variable tspace-top
variable /space         variable scan-ptr

hex
80000000 constant fwd-bit
40000000 constant byte-bit
0f000000 constant tag-mask
f0000000 constant bits-mask
00ffffff constant size-mask
decimal

variable gccount    gccount off
defer nonblock?  ( x -- f )      :noname  drop false ; is nonblock?

: >gcheader  ( a1 -- a2 ) 1 cells - ;
: gcheader  ( a -- x ) >gcheader @ ;
: available?  ( bytes -- f ) cell+ aligned fspace-top @ + fspace-limit @ < ;
: forward  ( old-1 new-1 -- ) over @ !+  1 rshift fwd-bit or swap ! ;

: gctracebytes  ( old-1 size -- new )
    over tspace-top @ forward  >r cell+ tspace-top @ cell+ r@ cmove 
    r> tspace-top @ cell+ dup >r + aligned tspace-top ! r> ;

: gctraceblock  ( old-1 size -- new )
    over tspace-top @ forward  cells >r cell+ tspace-top @ cell+ r@ cmove
    r> tspace-top @ cell+ dup >r + tspace-top ! r> ;

: gcblock?  ( a -- f ) 
  dup nonblock?  if  drop  false  exit  then
  fspace-start @ fspace-limit @ within ;
: +inspace  ( a -- a | *a ) dup gcblock?  0=  if  r> drop  then ;

: gctrace  ( old -- new )
    +inspace  1 cells - dup @ 
    dup fwd-bit and if  nip 1 lshift exit  then   \ forwarded?
    dup byte-bit and if  size-mask and gctracebytes exit  then \ byteblock?
    size-mask and gctraceblock ;           \ normal block

: swapspaces  ( -- )
    tspace-start @  tspace-limit @
    fspace-start @ tspace-start !  tspace-top @ fspace-top !
    fspace-limit @ tspace-limit !
    fspace-limit !  fspace-start !  tspace-start @ tspace-top ! 
    1 gccount +! ;

: gcinit  ( addr size -- )
    roots #roots @ cells erase
    2/ /space !  dup fspace-start !  
    /space @ + aligned dup fspace-limit !  dup tspace-start !
    /space @ + aligned tspace-limit !
    fspace-start @ fspace-top !
    tspace-start @ tspace-top !
    #roots off ;

: gcstart  tspace-top @ scan-ptr ! ;

: gctraceroots  
    #roots @ 0 ?do
        roots i th @ dup @ gctrace swap !
    loop ;

: gcscan
    scan-ptr @ begin
        dup tspace-top @ < while
        @+ dup byte-bit and if
            size-mask and + aligned
        else
            size-mask and 0 ?do
                dup @ gctrace over ! cell+
            loop
        then
    repeat drop ;

: gcend  swapspaces ;
: (gcreclaim)  gcstart  gctraceroots  gcscan  gcend ;
: gctotal  ( -- u ) fspace-limit @ fspace-start @ - ;
: gcfree  ( -- u ) fspace-limit @ fspace-top @ - ;

defer gcreclaim
' (gcreclaim) is gcreclaim

: gcalloc  ( bytes -- a ) \ a points behind header
    dup available? 0=  if
        gcreclaim  dup available? 0= abort" out of heap"
    then
    fspace-top @ 2dup + cell+ aligned fspace-top ! 
    2dup cell+ swap erase  swap 1 cells / !+ ;

: bytes  ( b -- b ) 
    dup 1 cells - dup @ dup size-mask and cells byte-bit or or 
    swap ! ;

\ tags should have the form h# 0X000000
: tagged  ( b tag -- b ) over 1 cells - dup @ rot or swap ! ;
: size  ( a -- n ) gcheader size-mask and ;
: tag  ( b -- tag ) gcheader tag-mask and ;
: bits  ( b -- bits ) gcheader bits-mask and ;
: bytes?  ( a -- f ) gcheader byte-bit and ;

40 constant /bar

: barscale  ( max n1 -- n2 ) /bar * swap / ;

: .space  ( top limit start -- )
    dup 8 u.r  ." →" 2dup - 3 pick rot  ( top limit total top start )
    - barscale dup >r [char] ▓ swap emits  ( top limit )
    swap 8 u.r  [char] ░ /bar r> - emits  
    ." ←"  8 u.r  cr ;

: gcroom  ( -- )
    base @ >r  hex
    ."  From: "  fspace-top @ fspace-limit @ fspace-start @ .space
    ."    To: "  tspace-top @ tspace-limit @ tspace-start @ .space 
    ." Roots: "  decimal  #roots ? cr 
    gccount ? ." GCs, " gcfree . ." bytes free, of " gctotal .
    r> base ! ;

: gcroot  ( a -- ) 
    #roots @ maxroots < 0= abort" too many gc roots"
    roots #roots @ th !  1 #roots +! ;

: gcvariable  ( | <word> -- ) ( -- a ) 
    variable  here 1 cells -  gcroot ;

: every?  ( b | ... -- f )
  r>  dup size 0  ?do  ( cb b ) 
    dup i th @ 2 pick callback  0=  if  
      unloop 2drop false  exit  then
  loop  2drop  true ;

: foreach  ( b | ... -- )
  r>  dup size 0  ?do  ( cb b )
    dup i th @ 2 pick callback
  loop  2drop ;
