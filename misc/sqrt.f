\ https://stackoverflow.com/questions/1100090/looking-for-an-efficient-integer-square-root-algorithm-for-arm-thumb2
variable op     variable res
: sqrt  ( u1 -- u2 ) op !  res off  1 1 cells 8 * 2 - lshift
  begin  dup op @ >  while  2 rshift  repeat
  begin  ?dup  while  ( one )
    dup res @ + op @ over >=  if  ( one res+one )
      dup op @ diff op !
      over + res !  
    else  drop  then
    res @ 2/ res !  2 rshift 
  repeat  res @ ;
