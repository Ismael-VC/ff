\ hashtable, using jenkings hash function
 \ https://en.wikipedia.org/wiki/Jenkins_hash_function

variable >table     variable #mask
begin-structure /entry
  field: entry.link    field: entry.name
end-structure
\ jenkins hash function
: hash  ( a u -- x )  0 -rot  0  ?do  count rot +  dup 10 lshift +
    dup 6 rshift xor  swap  loop  drop  dup 3 lshift +  dup 11
  rshift xor  dup 15 lshift + ;
: cleartable  >table @ #mask @ 1+ erase ;
: maketable  ( u -- ) \ must be power of 2
  dup r/w map-memory ?ior  >table !  
  1- #mask !  cleartable ;

: addentry  ( a u he -- entry ) here swap !  here >r  0 ,  
  string,  align  r> ;
: wraparound  ( a1 -- a2 ) >table @ - #mask @ and
  >table @ + ;
: findentry  ( a u he -- link ) begin  dup @ 
  0=  if  addentry  |  >r  2dup  r@ @ entry.name
  count compare 0=  if  2drop  r> @ entry.link  |
  r> cell+ wraparound  again ;
: lookup  ( a1 u -- a2 ) 2dup hash cells #mask @ and
  >table @ + findentry ;
