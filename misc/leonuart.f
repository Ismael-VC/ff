\ UART access for LEON experimental board

hex

80000100 constant UART_REG

1 constant UART_ST_DR       200 constant UART_ST_TF
1 constant UART_CR_RE       2 constant UART_CR_TE

: uart_reg  ( n -- a ) 4 * UART_REG + ;
: uart_data  0 uart_reg ;   : uart_status  1 uart_reg ;
: uart_ctrl  2 uart_reg ;   : uart_scaler  3 uart_reg ;
: uart_tf  ( -- f ) uart_status @ UART_ST_TF and ;
: uart_dr  ( -- f ) uart_status @ UART_ST_DR and ;

: uartout  ( a u -- )
  ?dup 0=  if  drop  exit  then
  UART_CR_RE UART_CR_TE or uart_data !  40 uart_scaler !
  0  do  begin  uart_tf 0 =  until
     dup c@ swap 1 + swap uart_data !  
  loop  drop ;

: uartin  ( -- c )
  UART_CR_RE UART_CR_TE or uart_data !  40 uart_scaler !
  begin  uart_dr  until  uart_data @ ;

: (conread)  ( channel a -- c 1|0 )
  uartin dup >r swap c!  drop  r> 1 ;

: (conwrite)  ( channel a u -- ) uartout  drop ;

' (conread) is conread      ' (conwrite) is conwrite

decimal
